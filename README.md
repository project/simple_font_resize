# Simple Font Resize
This module provides a simple font resize feature that should be added to sites to improve accessibility.

Module uses cookies to save users font-size so that it can be preset when user returns to the page.

This module supports EU Cookie Compliance. If user has not agreed to use cookies
font resize will work for current page.

## Requirements
This module requires that font size is set on `<html>` tag of the page.
All other font sizes must then use `rem` or `em` units.
