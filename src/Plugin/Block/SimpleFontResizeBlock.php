<?php

namespace Drupal\simple_font_resize\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SimpleFontResizeBlock' block.
 *
 * @Block(
 *  id = "simple_font_resize_block",
 *  admin_label = @Translation("Simple font resize block"),
 * )
 */
class SimpleFontResizeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'simple_font_resize',
      '#attached' => array(
        'library' => array(
          'simple_font_resize/main',
        ),
      ),
    ];
  }

}
