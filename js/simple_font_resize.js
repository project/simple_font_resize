(function ($, Drupal, drupalSettings) {
  'use strict';
  if ($('#simple-text-resize').length) {
    var allowed_to_use = true;
    if (Drupal.eu_cookie_compliance !== undefined) {
      allowed_to_use = Drupal.eu_cookie_compliance.hasAgreed();
    }

    if (allowed_to_use) {
      var cookie = getCookie('font-size');
      if (cookie) {
        $('html').css('font-size', cookie + 'px');
      } else {
        var size = parseInt($('html').css('font-size'));
        setCookie('font-size', size, 1);
      }
    }

    var font_increase = $('.font-increase');
    var font_reset = $('.font-reset');
    var font_decrease = $('.font-decrease');

    font_increase.on('click', function () {
      var size = parseInt($('html').css('font-size')) + 2;
      if (size <= 20) {
        $('html').css('font-size', size + 'px');
        if (allowed_to_use) {
          setCookie('font-size', size, 1);
        }
      }
    });
    font_reset.on('click', function () {
      $('html').css('font-size', 'initial');
      if (allowed_to_use) {
        deleteCookie('font-size');
      }
    });
    font_decrease.on('click', function () {
      var size = parseInt($('html').css('font-size')) - 2;
      if (size >= 10) {
        $('html').css('font-size', size + 'px');
        if (allowed_to_use) {
          setCookie('font-size', size, 1);
        }
      }
    });
  }
})(jQuery, Drupal, drupalSettings);

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function deleteCookie(cname) {
  document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
